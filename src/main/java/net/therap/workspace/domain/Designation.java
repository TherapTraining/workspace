package net.therap.workspace.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Entity
@Table(name = "designation")
public class Designation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @Size(min = 5, max = 100, message = "{label.designationError}")
    @Column(length = 250, nullable = false)
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNew() {
        return this.id == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Designation)) {
            return false;
        }

        Designation designation = (Designation) o;

        return (designation.getId() == this.id);
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
