package net.therap.workspace.domain;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 200, nullable = false)
    private String name;

    @NotBlank(message = "{emailNotBlank.message}")
    @Column(length = 100, nullable = false, updatable = false)
    private String email;

    @NotBlank(message = "{passwordNotBlank.message}")
    @Column(length = 500, nullable = false)
    private String password;

    @Column(length = 50, nullable = true, updatable = true, insertable = true, name = "picture_url")
    private String picture;

    @Column(length = 15, nullable = true, updatable = true, insertable = true, name = "phone_no")
    private String phone;

    @Column(length = 200, nullable = true, updatable = true, insertable = true)
    private String address;

    @Column(nullable = false, updatable = true, insertable = true, name = "is_admin")
    private int isAdmin;

    @Column(name = "create_date", updatable = false)
    private LocalDateTime createdTime;

    @OneToOne
    @JoinColumn(name = "created_by", updatable = false)
    private User creator;

    @Column(name = "last_update_date", insertable = false)
    private LocalDateTime lastUpdateDate;

    @OneToOne
    @JoinColumn(name = "last_updated_by", insertable = false)
    private User lastUpdater;

    @Column(name = "deleted_date", insertable = false)
    private LocalDateTime deletedTime;

    @OneToOne
    @JoinColumn(name = "deleted_by", insertable = false)
    private User deleter;

    @Column
    private int status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(
            name = "team_user",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "team_id")
    )
    private Team team;

    @OneToOne
    @JoinColumn(name = "designation_id")
    private Designation designation;

    @OneToOne(fetch = FetchType.EAGER, mappedBy = "teamLead")
    private Team ledTeam;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "user_created_post",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "post_id"))
    private List<Post> createdPosts;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "user_tagged_post",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "post_id"))
    private List<Post> taggedPosts;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "user_invited_meeting",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "meeting_id"))
    private List<Meeting> invitedMeetings;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "user_created_meeting",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "meeting_id"))
    private List<Meeting> createdMeetings;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "user_assigned_task",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id")
    )
    private List<Task> assignedTasks;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(
            name = "user_created_task",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id")
    )
    private List<Task> createdTasks;

    public User() {
        this.createdPosts = new ArrayList<>();
        this.taggedPosts = new ArrayList<>();
        this.invitedMeetings = new ArrayList<>();
        this.createdMeetings = new ArrayList<>();
        this.createdTasks = new ArrayList<>();
        this.assignedTasks = new ArrayList<>();
    }

    public Team getLedTeam() {
        return ledTeam;
    }

    public void setLedTeam(Team leadedTeam) {
        this.ledTeam = leadedTeam;
    }

    public boolean isNew() {
        return this.getId() == 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Designation getDesignation() {
        return designation;
    }

    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public LocalDateTime getDeletedTime() {
        return deletedTime;
    }

    public void setDeletedTime(LocalDateTime deletedTime) {
        this.deletedTime = deletedTime;
    }

    public LocalDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public User getDeleter() {
        return deleter;
    }

    public void setDeleter(User deleter) {
        this.deleter = deleter;
    }

    public User getLastUpdater() {
        return lastUpdater;
    }

    public void setLastUpdater(User lastUpdater) {
        this.lastUpdater = lastUpdater;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Post> getCreatedPosts() {
        return createdPosts;
    }

    public void setCreatedPosts(List<Post> createdPosts) {
        this.createdPosts = createdPosts;
    }

    public List<Post> getTaggedPosts() {
        return taggedPosts;
    }

    public void setTaggedPosts(List<Post> taggedPosts) {
        this.taggedPosts = taggedPosts;
    }

    public List<Meeting> getInvitedMeetings() {
        return invitedMeetings;
    }

    public void setInvitedMeetings(List<Meeting> meetings) {
        this.invitedMeetings = meetings;
    }

    public List<Meeting> getCreatedMeetings() {
        return createdMeetings;
    }

    public void setCreatedMeetings(List<Meeting> createdMeetings) {
        this.createdMeetings = createdMeetings;
    }

    public List<Task> getAssignedTasks() {
        return assignedTasks;
    }

    public void setAssignedTasks(List<Task> assignedTasks) {
        this.assignedTasks = assignedTasks;
    }

    public List<Task> getCreatedTasks() {
        return createdTasks;
    }

    public void setCreatedTasks(List<Task> createdTasks) {
        this.createdTasks = createdTasks;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof User)) {
            return false;
        }

        User user = (User) o;
        return (user.getId() == this.id);
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public String toString() {
        return this.name;
    }
}