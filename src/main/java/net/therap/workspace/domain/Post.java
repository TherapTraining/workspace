package net.therap.workspace.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Entity
@Table(name = "post")
public class Post implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "{titleNotBlank.message}")
    private String title;

    @NotBlank(message = "{bodyNotBlank.message}")
    private String body;

    @Column(name = "create_date")
    private LocalDateTime createdTime;

    @OneToOne
    @JoinColumn(name = "created_by")
    private User creator;

    @Column(name = "last_update_date")
    private LocalDateTime lastUpdateDate;

    @OneToOne
    @JoinColumn(name = "last_updated_by")
    private User lastUpdater;

    @Column(name = "deleted_date")
    private LocalDateTime deletedTime;

    @OneToOne
    @JoinColumn(name = "deleted_by")
    private User deleter;

    @Column(name = "file")
    private String filePath;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_tagged_post",
            joinColumns = @JoinColumn(name = "post_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> taggedUsers;

    private int status;

    public Post() {
        this.taggedUsers = new ArrayList<>();
    }

    public boolean isNew() {
        return this.getId() == 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public LocalDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public User getLastUpdater() {
        return lastUpdater;
    }

    public void setLastUpdater(User lastUpdater) {
        this.lastUpdater = lastUpdater;
    }

    public LocalDateTime getDeletedTime() {
        return deletedTime;
    }

    public void setDeletedTime(LocalDateTime deletedTime) {
        this.deletedTime = deletedTime;
    }

    public User getDeleter() {
        return deleter;
    }

    public void setDeleter(User deleter) {
        this.deleter = deleter;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<User> getTaggedUsers() {
        return taggedUsers;
    }

    public void setTaggedUsers(List<User> taggedUsers) {
        this.taggedUsers = taggedUsers;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Post)) {
            return false;
        }

        Post post = (Post) o;

        return (post.getId() == this.id);
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
