package net.therap.workspace.domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Entity
@Table(name = "meeting")
public class Meeting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column(name = "time", nullable = false, updatable = true, insertable = true)
    private LocalDateTime time;

    @NotBlank(message = "{titleNotBlank.message}")
    @Column(name = "title", nullable = false, updatable = true, insertable = true, length = 1000)
    private String title;

    @NotBlank(message = "{agendaNotBlank.message}")
    @Column(name = "agenda", nullable = false, updatable = true, insertable = true, length = 2000)
    private String agenda;

    @NotBlank(message = "{urlNotBlank.message}")
    @Size(min = 3, max = 100, message = "{sizeError.message}")
    @Column(name = "url", nullable = false, updatable = true, insertable = true, length = 100)
    private String url;

    @Column(name = "create_date")
    private LocalDateTime createdTime;

    @OneToOne
    @JoinColumn(name = "created_by")
    private User creator;

    @Column(name = "last_update_date")
    private LocalDateTime lastUpdateTime;

    @OneToOne
    @JoinColumn(name = "last_updated_by")
    private User lastUpdater;

    @Column(name = "deleted_date")
    private LocalDateTime deletedTime;

    @OneToOne
    @JoinColumn(name = "deleted_by")
    private User deleter;

    private int status;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_invited_meeting",
            joinColumns = @JoinColumn(name = "meeting_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> guests;

    public Meeting() {
        this.guests = new ArrayList<>();
    }

    public boolean isNew() {
        return this.getId() == 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public LocalDateTime getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(LocalDateTime lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public User getLastUpdater() {
        return lastUpdater;
    }

    public void setLastUpdater(User lastUpdater) {
        this.lastUpdater = lastUpdater;
    }

    public LocalDateTime getDeletedTime() {
        return deletedTime;
    }

    public void setDeletedTime(LocalDateTime deletedTime) {
        this.deletedTime = deletedTime;
    }

    public User getDeleter() {
        return deleter;
    }

    public void setDeleter(User deleter) {
        this.deleter = deleter;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<User> getGuests() {
        return guests;
    }

    public void setGuests(List<User> guests) {
        this.guests = guests;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Meeting)) {
            return false;
        }

        Meeting meeting = (Meeting) o;

        return (meeting.getId() == this.id);
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}