package net.therap.workspace.dao;

import net.therap.workspace.domain.Task;
import net.therap.workspace.domain.Team;
import net.therap.workspace.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 22/02/2021
 **/
@Repository
public class TeamDao {

    private static final int STATUS = 0;

    @PersistenceContext
    private EntityManager em;

    public Team findById(int id) {
        return em.find(Team.class, id);
    }

    public List<Task> getTaskListUser(Team team, User user) {
        TypedQuery<Task> query = em.createQuery("SELECT task FROM Task task WHERE task.team = :team AND task.status = :status", Task.class);
        query.setParameter("status", STATUS);
        query.setParameter("team", team);

        List<Task> allTaskList = query.getResultList();
        List<Task> taskList = new ArrayList<>();
        for (Task task : allTaskList) {
            if ((task.getCreatedBy().equals(user) || task.getAssignedUsers().contains(user))) {
                taskList.add(task);
            }
        }

        return taskList;
    }
}
