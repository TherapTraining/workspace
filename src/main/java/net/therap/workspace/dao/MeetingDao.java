package net.therap.workspace.dao;

import net.therap.workspace.domain.Meeting;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Component
public class MeetingDao {

    @PersistenceContext
    private EntityManager em;

    public Meeting getMeetingById(int id) {
        return em.find(Meeting.class, id);
    }

    @Transactional
    public void save(Meeting meeting) {
        if (meeting.isNew()) {
            em.persist(meeting);
        } else {
            em.merge(meeting);
        }
    }
}
