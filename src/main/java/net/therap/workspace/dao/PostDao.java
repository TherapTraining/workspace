package net.therap.workspace.dao;

import net.therap.workspace.domain.Post;
import net.therap.workspace.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Component
public class PostDao {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Post> getPosts() {
        TypedQuery<Post> query =
                entityManager.createQuery("SELECT post FROM Post post ORDER BY post.id", Post.class);
        return query.getResultList();
    }

    public Post getPost(Post post) {
        return entityManager.find(Post.class, post.getId());
    }

    public List<Post> getUserCreatedPosts(User user) {
        TypedQuery<Post> query =
                entityManager.createQuery("SELECT post FROM Post post WHERE post.creator = :user ", Post.class);
        query.setParameter("user", user);
        return query.getResultList();
    }

    @Transactional
    public void save(Post post) {
        if (post.getId() == 0) {
            entityManager.persist(post);
        } else {
            entityManager.merge(post);
        }
    }
}
