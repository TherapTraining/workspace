package net.therap.workspace.dao;

import net.therap.workspace.domain.Designation;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Component
public class DesignationDao {

    @PersistenceContext
    private EntityManager em;

    public List<Designation> getDesignations() {
        TypedQuery<Designation> query =
                em.createQuery("SELECT designation FROM Designation designation ORDER BY designation.id", Designation.class);
        return query.getResultList();
    }

    @Transactional
    public void save(Designation designation) {
        if (designation.getId() == 0) {
            em.persist(designation);
        } else {
            em.merge(designation);
        }
    }
}
