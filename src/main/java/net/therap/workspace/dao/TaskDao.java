package net.therap.workspace.dao;

import net.therap.workspace.domain.Task;
import net.therap.workspace.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 21/02/2021
 **/
@Repository
public class TaskDao {

    private static final int STATUS = 0;

    @PersistenceContext
    private EntityManager em;

    public Task findById(int id) {
        return em.find(Task.class, id);
    }

    public List<Task> findByIsCompleted(int isCompleted, User user) {
        TypedQuery<Task> query = em.createQuery("SELECT task FROM Task task WHERE task.isCompleted = :isCompleted AND task.status = :status", Task.class);
        query.setParameter("isCompleted", isCompleted);
        query.setParameter("status", STATUS);

        List<Task> allTaskList = query.getResultList();
        List<Task> taskList = new ArrayList<>();
        for (Task task : allTaskList) {
            if (task.getAssignedUsers().contains(user)) {
                taskList.add(task);
            }
        }

        return taskList;
    }

    public List<Task> findAssigned(User user) {
        TypedQuery<Task> query = em.createQuery("SELECT task FROM Task task WHERE task.createdBy = :createdBy AND task.status = :status", Task.class);
        query.setParameter("createdBy", user);
        query.setParameter("status", STATUS);

        return query.getResultList();
    }

    @Transactional
    public Task saveOrUpdate(Task task) {

        if (task.isNew()) {
            em.persist(task);
        } else {
            return em.merge(task);
        }
        em.flush();

        return task;
    }
}
