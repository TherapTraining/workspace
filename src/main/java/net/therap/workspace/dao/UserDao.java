package net.therap.workspace.dao;

import net.therap.workspace.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Component
public class UserDao {

    @PersistenceContext
    private EntityManager em;

    private static boolean validatePassword(String originalPassword,
                                            String storedPassword)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        String[] parts = storedPassword.split(":");
        int iterations = Integer.parseInt(parts[0]);
        byte[] salt = fromHex(parts[1]);
        byte[] hash = fromHex(parts[2]);

        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        int diff = hash.length ^ testHash.length;
        for (int i = 0; i < hash.length && i < testHash.length; i++) {
            diff |= hash[i] ^ testHash[i];
        }
        return diff == 0;
    }

    private static byte[] fromHex(String hex) {
        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

    public List<User> getUsers() {
        TypedQuery<User> query =
                em.createQuery("SELECT user FROM User user ORDER BY user.name", User.class);
        return query.getResultList();
    }

    public User getUserByEmail(String email) {
        TypedQuery<User> query =
                em.createQuery("SELECT user FROM User user WHERE user.email = :email", User.class);
        query.setParameter("email", email);
        return query.getSingleResult();
    }

    public User getUserById(int id) {
        return em.find(User.class, id);
    }

    public boolean authenticateUser(User user) throws InvalidKeySpecException, NoSuchAlgorithmException {
        TypedQuery<User> query =
                em.createQuery("SELECT user FROM User user WHERE user.email = :email", User.class);
        query.setParameter("email", user.getEmail());
        List<User> users = query.getResultList();
        if (users.isEmpty()) {
            return false;
        } else {
            User currentUser = users.get(0);
            return validatePassword(user.getPassword(), currentUser.getPassword());
        }
    }

    @Transactional
    public void save(User user) throws InvalidKeySpecException, NoSuchAlgorithmException {
        if (user.isNew()) {
            user.setPassword(generateHash(user.getPassword()));
            em.persist(user);
        } else {
            em.merge(user);
        }
    }

    public String generateHash(String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        {
            int iterations = 1000;
            char[] chars = password.toCharArray();
            byte[] salt = getSalt();

            PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();
            return iterations + ":" + toHex(salt) + ":" + toHex(hash);
        }
    }
}
