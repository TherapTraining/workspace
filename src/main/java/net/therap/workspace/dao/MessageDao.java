package net.therap.workspace.dao;

import net.therap.workspace.domain.Message;
import net.therap.workspace.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 03/03/2021
 **/
@Repository
public class MessageDao {

    private static final int MESSAGE_STATUS_ACTIVE = 0;

    @PersistenceContext
    private EntityManager em;

    public List<Message> getChat(User currentUser, User otherUser) {
        TypedQuery<Message> query = em.createQuery("SELECT message FROM Message message WHERE " +
                "( (message.sender = :currentUser AND message.receiver = :otherUser) OR " +
                "(message.sender = :otherUser AND message.receiver = :currentUser) ) AND " +
                "message.status = :status", Message.class);

        query.setParameter("currentUser", currentUser)
                .setParameter("otherUser", otherUser)
                .setParameter("status", MESSAGE_STATUS_ACTIVE);

        return query.getResultList();
    }

    @Transactional
    public Message saveOrUpdate(Message message) {

        if (message.isNew()) {
            em.persist(message);
        } else {
            return em.merge(message);
        }
        em.flush();

        return message;
    }
}
