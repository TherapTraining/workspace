package net.therap.workspace.service;

import net.therap.workspace.dao.MessageDao;
import net.therap.workspace.domain.Message;
import net.therap.workspace.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author aunabil.chakma
 * @since 03/03/2021
 **/
@Service
public class MessageService {

    @Autowired
    private MessageDao messageDao;

    public List<Message> getChat(User currentUser, User otherUser) {
        return messageDao.getChat(currentUser, otherUser);
    }

    public Message saveOrUpdate(Message message) {
        return messageDao.saveOrUpdate(message);
    }
}
