package net.therap.workspace.service;

import net.therap.workspace.dao.TeamDao;
import net.therap.workspace.domain.Task;
import net.therap.workspace.domain.Team;
import net.therap.workspace.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author aunabil.chakma
 * @since 22/02/2021
 **/
@Service
public class TeamService {

    @Autowired
    private TeamDao teamDao;

    public Team findById(int id) {
        if (id == 0) {
            return null;
        }
        return teamDao.findById(id);
    }

    public List<Task> getTaskListUser(Team team, User user) {
        return teamDao.getTaskListUser(team, user);
    }
}
