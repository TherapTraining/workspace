package net.therap.workspace.service;

import net.therap.workspace.dao.TaskDao;
import net.therap.workspace.domain.Task;
import net.therap.workspace.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 21/02/2021
 **/
@Service
public class TaskService {

    @Autowired
    private TaskDao taskDao;

    public Task findById(int id) {
        return taskDao.findById(id);
    }

    public List<Task> findByIsCompleted(int status, User user) {
        return taskDao.findByIsCompleted(status, user);
    }

    public List<Task> findAssigned(User user) {
        return taskDao.findAssigned(user);
    }

    @Transactional
    public Task saveOrUpdate(Task task) {
        return taskDao.saveOrUpdate(task);
    }
}
