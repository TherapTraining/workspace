package net.therap.workspace.editor;

import net.therap.workspace.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Service
public class UserEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        User user = (User) getValue();
        if (Objects.isNull(user)) {
            return "";
        }
        return String.valueOf(user.getId());
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            User user = new User();
            user.setId(Integer.parseInt(idTxt));
            setValue(user);
        }
    }
}
