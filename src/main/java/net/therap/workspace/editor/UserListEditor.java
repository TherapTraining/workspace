package net.therap.workspace.editor;

import net.therap.workspace.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Service
public class UserListEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        List<User> users = (List) getValue();
        StringBuilder usersTxt = new StringBuilder();
        for (User user : users) {
            usersTxt.append(user.getId());
            if (users.indexOf(user) != users.size() - 1) {
                usersTxt.append(",");
            }
        }
        return usersTxt.toString();
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        List<User> userList = new ArrayList<>();
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            String[] users = idTxt.split(",");
            for (String userStr : users) {
                User user = new User();
                user.setId(Integer.parseInt(userStr));
                userList.add(user);
            }
            setValue(userList);
        }
    }
}