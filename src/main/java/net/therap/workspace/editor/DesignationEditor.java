package net.therap.workspace.editor;

import net.therap.workspace.domain.Designation;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.Objects;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Service
public class DesignationEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        Designation designation = (Designation) getValue();
        if (Objects.isNull(designation)) {
            return "";
        }
        return String.valueOf(designation.getId());
    }

    @Override
    public void setAsText(String idTxt) throws IllegalArgumentException {
        if (StringUtils.isEmpty(idTxt)) {
            setValue(null);
        } else {
            Designation designation = new Designation();
            designation.setId(Integer.parseInt(idTxt));
            setValue(designation);
        }
    }
}
