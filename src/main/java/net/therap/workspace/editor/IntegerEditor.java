package net.therap.workspace.editor;

import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

/**
 * @author aunabil.chakma
 * @since 24/02/2021
 **/
@Component
public class IntegerEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        Integer integer = (Integer) getValue();
        return integer == null ? "" : integer.toString();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        setValue(Integer.parseInt(text));
    }
}
