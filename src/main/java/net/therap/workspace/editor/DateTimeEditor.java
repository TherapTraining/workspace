package net.therap.workspace.editor;

import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author aunabil.chakma
 * @since 21/02/2021
 **/
@Component
public class DateTimeEditor extends PropertyEditorSupport {

    @Override
    public String getAsText() {
        LocalDateTime dateTime = (LocalDateTime) getValue();
        return dateTime == null ? "" : dateTime.toString();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        LocalDateTime dateTime = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        if (text != null && !text.isEmpty()) {
            dateTime = LocalDateTime.parse(text, formatter);
        }
        setValue(dateTime);
    }
}