package net.therap.workspace.validator;

import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Component
public class UserValidator implements Validator {

    @Autowired
    private UserDao userDao;

    public boolean supports(Class clazz) {
        return User.class.equals(clazz);
    }

    public void validate(Object obj, Errors e) {
        User user = (User) obj;
        try {
            if (!userDao.authenticateUser(user)) {
                e.rejectValue("email", "wrong.credentials", "{emailNotBlank.message}");
            }
        } catch (InvalidKeySpecException | NoSuchAlgorithmException invalidKeySpecException) {
            invalidKeySpecException.printStackTrace();
        }
    }
}
