package net.therap.workspace.filter;

import net.therap.workspace.domain.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
public class AuthFilter implements Filter {

    private static final String USER = "currentUser";

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpSession session = httpReq.getSession(false);
        if (session == null && (httpReq.getServletPath().equals("/login") || httpReq.getServletPath().equals("/"))) {
            chain.doFilter(request, response);
        } else if (session == null && !httpReq.getServletPath().equals("/login") && !httpReq.getServletPath().equals("/")) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/");
            dispatcher.forward(request, response);
        } else if (session != null && (httpReq.getServletPath().equals("/login") || httpReq.getServletPath().equals("/"))) {
            User user = (User) session.getAttribute(USER);
            if (user == null) {
                session.invalidate();
                chain.doFilter(request, response);
            } else {
                RequestDispatcher dispatcher = request.getRequestDispatcher("/home");
                dispatcher.forward(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    public void init(FilterConfig config) {

    }
}
