package net.therap.workspace.controller;

import net.therap.workspace.dao.PostDao;
import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.Post;
import net.therap.workspace.domain.User;
import net.therap.workspace.editor.UserEditor;
import net.therap.workspace.editor.UserListEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Controller
public class PostController {

    private static final String POST_CREATE_PAGE = "createPost";
    private static final String POST_SENT_PAGE = "viewSentPosts";
    private static final String POST_INBOX_PAGE = "postInbox";
    private static final String POST_PAGE = "post";
    private static final String USER = "currentUser";
    private static final String VIEW_SINGLE_POST = "viewSinglePost";
    private static final String REDIRECT_SINGLE_POST = "viewSinglePost";
    private static final String REDIRECT_SENT_POST = "redirect:/postSent";
    private static final String UPLOAD_DIRECTORY = "/images";
    private static final int POST_STATUS_ACTIVE = 1;
    private static final int POST_STATUS_DELETED = 0;

    @Autowired
    private UserDao userDao;

    @Autowired
    private PostDao postDao;

    @Autowired
    private UserEditor userEditor;

    @Autowired
    private UserListEditor userListEditor;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, userListEditor);
        binder.registerCustomEditor(User.class, userEditor);
    }

    @RequestMapping("/post")
    public String viewPost() {
        return POST_PAGE;
    }

    private ModelMap setupPost(ModelMap model, HttpSession session, Post post) {
        if (post.getFilePath() != null) {
            String[] fileProperties = post.getFilePath().split(",");
            model.addAttribute("filePath", fileProperties[0]);
            model.addAttribute("fileName", fileProperties[1]);
        } else {
            model.addAttribute("file", null);
        }
        List<User> users = userDao.getUsers();
        User user = (User) session.getAttribute(USER);
        users.removeIf(currentUser -> currentUser.getId() == user.getId());
        model.addAttribute("post", post);
        model.addAttribute("users", users);
        model.addAttribute("user", user);
        model.addAttribute("status", POST_STATUS_ACTIVE);
        return model;
    }

    @RequestMapping("/postCreate")
    public String createPost(Post post, ModelMap model, HttpSession session) {
        if (post.isNew()) {
            post = new Post();
        } else {
            post = postDao.getPost(post);
        }
        model = setupPost(model, session, post);
        return POST_CREATE_PAGE;
    }

    @RequestMapping(value = "/deletePost", method = RequestMethod.POST)
    public String deletePost(Post post, HttpSession session) {
        User activeUser = (User) session.getAttribute(USER);
        User deleter = userDao.getUserById(activeUser.getId());
        post = postDao.getPost(post);
        post.setStatus(POST_STATUS_DELETED);
        post.setDeletedTime(LocalDateTime.now());
        post.setDeleter(deleter);
        postDao.save(post);
        return REDIRECT_SENT_POST;
    }

    @RequestMapping("/postInbox")
    public String postInbox(ModelMap model, HttpSession session) {
        User user = (User) session.getAttribute(USER);
        user = userDao.getUserById(user.getId());
        List<Post> posts = user.getTaggedPosts();
        posts.removeIf(post -> post.getStatus() != POST_STATUS_ACTIVE);
        model.addAttribute("posts", posts);
        return POST_INBOX_PAGE;
    }

    @RequestMapping("/viewSinglePost")
    public String viewSinglePost(Post post, ModelMap model) {
        post = postDao.getPost(post);
        if (post.getFilePath() != null) {
            String[] fileProperties = post.getFilePath().split(",");
            model.addAttribute("filePath", fileProperties[0]);
            model.addAttribute("fileName", fileProperties[1]);
        }
        model.addAttribute("post", post);
        return VIEW_SINGLE_POST;
    }

    @RequestMapping("/downloadPostFile")
    public String download(Post post, @RequestParam String filePath, @RequestParam String fileName,
                           HttpServletResponse response, ModelMap model) throws IOException {
        Path path = Paths.get(filePath, fileName);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (Files.exists(path)) {
            response.setContentType("application/octet-stream");
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            FileInputStream fileInputStream = new FileInputStream(filePath + "/" + fileName);
            int i;
            while ((i = fileInputStream.read()) != -1) {
                out.write(i);
            }
            fileInputStream.close();
            out.close();
        }
        post = postDao.getPost(post);
        model.addAttribute("post", post);
        return REDIRECT_SINGLE_POST;
    }

    @RequestMapping(value = "/deletePostFile")
    public String deleteFile(Post post, ModelMap model, HttpSession session, @RequestParam String filePath,
                             @RequestParam String fileName) {
        post = postDao.getPost(post);
        post.setFilePath(null);
        postDao.save(post);
        Path file = Paths.get(filePath, fileName);
        if (Files.exists(file)) {
            try {
                Files.delete(file);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        model = setupPost(model, session, post);
        return createPost(post, model, session);
    }

    @RequestMapping("/postSent")
    public String viewSentPosts(ModelMap model, HttpSession session) {
        User activeUser = (User) session.getAttribute(USER);
        User user = userDao.getUserById(activeUser.getId());
        List<Post> posts = user.getCreatedPosts();
        model.addAttribute("posts", posts);
        return POST_SENT_PAGE;
    }

    @RequestMapping(value = "/savePost", method = RequestMethod.POST)
    public String savePost(@Valid Post post, BindingResult result, HttpSession session,
                           ModelMap model, @RequestParam("file") CommonsMultipartFile file)
            throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        boolean newPost;
        newPost = post.getId() == 0;
        if (result.hasErrors()) {
            model = setupPost(model, session, post);
            return POST_CREATE_PAGE;
        } else {
            if (newPost) {
                post.setCreatedTime(LocalDateTime.now());
            } else {
                post.setCreatedTime(postDao.getPost(post).getCreatedTime());
            }
            if (!file.isEmpty() && post.getFilePath() == null) {
                ServletContext context = session.getServletContext();
                String path = context.getRealPath(UPLOAD_DIRECTORY);
                String filename = file.getOriginalFilename();
                byte[] bytes = file.getBytes();
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
                        new File(path + File.separator + filename)));
                stream.write(bytes);
                stream.flush();
                stream.close();
                post.setFilePath(path + "," + filename);
            }
            post.setLastUpdateDate(LocalDateTime.now());
            postDao.save(post);
            User activeUser = (User) session.getAttribute(USER);
            User user = userDao.getUserById(activeUser.getId());
            List<Post> createdPosts = user.getCreatedPosts();
            if (!createdPosts.contains(post)) {
                createdPosts.add(post);
            }
            userDao.save(user);
            if (newPost) {
                return POST_PAGE;
            } else {
                return REDIRECT_SENT_POST;
            }
        }
    }
}
