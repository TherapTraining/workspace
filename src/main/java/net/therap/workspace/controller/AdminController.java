package net.therap.workspace.controller;

import net.therap.workspace.dao.DesignationDao;
import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.Designation;
import net.therap.workspace.domain.User;
import net.therap.workspace.editor.DesignationEditor;
import net.therap.workspace.editor.IntegerEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    private static final String USER = "currentUser";
    private static final String USER_CREATE_PAGE = "newUser";
    private static final String USER_SHOW_PAGE = "showUsers";
    private static final String DESIGNATION_CREATE_PAGE = "newDesignation";
    private static final String REDIRECT_HOME = "redirect:/home";
    private static final String REDIRECT_ACTIVE_USERS = "redirect:/admin/activeUsers";
    private static final int TRUE_ADMIN = 1;
    private static final int ACTIVE = 1;
    private static final int DELETED = 0;

    @Autowired
    private UserDao userDao;

    @Autowired
    private DesignationDao designationDao;

    @Autowired
    private DesignationEditor designationEditor;

    @Autowired
    private IntegerEditor integerEditor;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Designation.class, "designation", designationEditor);
        binder.registerCustomEditor(Integer.class, "isAdmin", integerEditor);
    }

    @GetMapping("/createUser")
    public String newUser(User newUser, HttpSession session, ModelMap model) {
        User user = (User) session.getAttribute(USER);
        user = userDao.getUserById(user.getId());
        if (user.getIsAdmin() == TRUE_ADMIN) {
            if (newUser.isNew()) {
                newUser = new User();
            } else {
                newUser = userDao.getUserById(newUser.getId());
            }
            List<Designation> designations = designationDao.getDesignations();
            model.addAttribute("user", newUser);
            model.addAttribute("designations", designations);
            return USER_CREATE_PAGE;
        } else {
            return REDIRECT_HOME;
        }
    }

    @GetMapping("/createDesignation")
    public String newDesignation(ModelMap model) {
        Designation designation = new Designation();
        model.addAttribute("designation", designation);
        return DESIGNATION_CREATE_PAGE;
    }

    @PostMapping("/saveDesignation")
    public String saveDesignation(@Valid Designation designation, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            model.addAttribute("designation", designation);
            return DESIGNATION_CREATE_PAGE;
        }
        designationDao.save(designation);
        return REDIRECT_HOME;
    }

    @PostMapping("/saveUser")
    private String saveUser(@Valid User user, BindingResult result, HttpSession session, ModelMap model)
            throws InvalidKeySpecException, NoSuchAlgorithmException {
        if (result.hasErrors()) {
            model.addAttribute("user", user);
            List<Designation> designations = designationDao.getDesignations();
            model.addAttribute("designations", designations);
            return USER_CREATE_PAGE;
        }
        User admin = (User) session.getAttribute(USER);
        admin = userDao.getUserById(admin.getId());
        if (user.isNew()) {
            user.setCreatedTime(LocalDateTime.now());
            user.setCreator(admin);
            user.setStatus(ACTIVE);
        } else {
            user.setLastUpdateDate(LocalDateTime.now());
            user.setLastUpdater(admin);
            user.setStatus(ACTIVE);
        }
        userDao.save(user);
        return REDIRECT_HOME;
    }

    @GetMapping("/activeUsers")
    public String showUserList(HttpSession session, ModelMap model) {
        User user = (User) session.getAttribute(USER);
        user = userDao.getUserById(user.getId());
        if (user.getIsAdmin() == TRUE_ADMIN) {
            List<User> users = userDao.getUsers();
            users.removeIf(currentUser -> currentUser.getStatus() != ACTIVE);
            model.addAttribute("users", users);
            return USER_SHOW_PAGE;
        } else {
            return REDIRECT_HOME;
        }
    }

    @PostMapping("/deleteUser")
    private String deleteUser(User user, HttpSession session)
            throws InvalidKeySpecException, NoSuchAlgorithmException {
        User admin = (User) session.getAttribute(USER);
        admin = userDao.getUserById(admin.getId());
        user = userDao.getUserById(user.getId());
        user.setStatus(DELETED);
        user.setDeletedTime(LocalDateTime.now());
        user.setDeleter(admin);
        userDao.save(user);
        return REDIRECT_ACTIVE_USERS;
    }
}
