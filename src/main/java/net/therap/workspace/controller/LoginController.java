package net.therap.workspace.controller;

import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.User;
import net.therap.workspace.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Controller
public class LoginController {

    private static final String LOGIN_PAGE = "login";
    private static final String HOME_PAGE = "home";
    private static final String REDIRECT_LOGIN = "redirect:/";
    private static final String REDIRECT_HOME = "redirect:/home";
    private static final String USER = "currentUser";

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserValidator userValidator;

    @InitBinder
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(userValidator);
    }

    @RequestMapping({"/", "/login"})
    public String helloWorld(ModelMap model) {
        User user = new User();
        model.addAttribute("user", user);
        return LOGIN_PAGE;
    }

    @RequestMapping("/home")
    public String viewHome(ModelMap model, HttpSession session) {
        User user = (User) session.getAttribute(USER);
        user = userDao.getUserById(user.getId());
        model.addAttribute("user", user);
        return HOME_PAGE;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String newDesignation(@Valid User user, BindingResult result, HttpServletRequest request) {
        if (result.hasErrors()) {
            return LOGIN_PAGE;
        }
        user = userDao.getUserByEmail(user.getEmail());
        request.getSession(true).setAttribute(USER, user);
        return REDIRECT_HOME;

    }

    @RequestMapping(value = "/logout")
    public String logOut(HttpSession session) {
        session.invalidate();
        return REDIRECT_LOGIN;
    }
}
