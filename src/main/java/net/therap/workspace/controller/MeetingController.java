package net.therap.workspace.controller;

import net.therap.workspace.dao.MeetingDao;
import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.Meeting;
import net.therap.workspace.domain.User;
import net.therap.workspace.editor.DateTimeEditor;
import net.therap.workspace.editor.UserEditor;
import net.therap.workspace.editor.UserListEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Controller
public class MeetingController {

    private static final String MEETING_CREATE_PAGE = "createMeeting";
    private static final String MEETING_CREATED_PAGE = "createdMeetings";
    private static final String MEETING_INVITES_PAGE = "invitedMeetings";
    private static final String REDIRECT_CREATED_MEETING = "redirect:/meetingCreated";
    private static final String USER = "currentUser";
    private static final String HTTP = "http://";
    private static final int ACTIVE = 1;
    private static final int DELETED = 0;

    @Autowired
    private MeetingDao meetingDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserEditor userEditor;

    @Autowired
    private UserListEditor userListEditor;

    @Autowired
    private DateTimeEditor dateTimeEditor;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, userListEditor);
        binder.registerCustomEditor(User.class, userEditor);
        binder.registerCustomEditor(LocalDateTime.class, dateTimeEditor);
    }

    @RequestMapping("/meetingCreate")
    public String createMeeting(Meeting meeting, ModelMap model, HttpSession session) {
        if (meeting.isNew()) {
            meeting = new Meeting();
        } else {
            meeting = meetingDao.getMeetingById(meeting.getId());
        }
        model = initialize(model, session, meeting);
        return MEETING_CREATE_PAGE;
    }

    @RequestMapping(value = "/deleteMeeting", method = RequestMethod.POST)
    public String deleteMeeting(Meeting meeting, HttpSession session) {
        User activeUser = (User) session.getAttribute(USER);
        User deleter = userDao.getUserById(activeUser.getId());
        meeting = meetingDao.getMeetingById(meeting.getId());
        meeting.setStatus(DELETED);
        meeting.setDeletedTime(LocalDateTime.now());
        meeting.setDeleter(deleter);
        meetingDao.save(meeting);
        return REDIRECT_CREATED_MEETING;
    }

    @RequestMapping("/invitedMeeting")
    public String viewInvitedMeetings(ModelMap model, HttpSession session) {
        User user = (User) session.getAttribute(USER);
        user = userDao.getUserById(user.getId());
        List<Meeting> meetings = user.getInvitedMeetings();
        meetings.removeIf(meeting -> meeting.getStatus() != ACTIVE);
        model.addAttribute("meetings", meetings);
        return MEETING_INVITES_PAGE;
    }

    @RequestMapping("/meetingCreated")
    public String viewCreatedMeetings(ModelMap model, HttpSession session) {
        User activeUser = (User) session.getAttribute(USER);
        User user = userDao.getUserById(activeUser.getId());
        List<Meeting> meetings = user.getCreatedMeetings();
        model.addAttribute("meetings", meetings);
        return MEETING_CREATED_PAGE;
    }

    private ModelMap initialize(ModelMap model, HttpSession session, Meeting meeting) {
        List<User> users = userDao.getUsers();
        User user = (User) session.getAttribute(USER);
        users.removeIf(currentUser -> currentUser.getId() == user.getId());
        model.addAttribute("meeting", meeting);
        model.addAttribute("users", users);
        model.addAttribute("user", user);
        model.addAttribute("status", ACTIVE);
        return model;
    }

    @RequestMapping(value = "/saveMeeting", method = RequestMethod.POST)
    public String saveMeeting(@Valid Meeting meeting, BindingResult result, HttpSession session, ModelMap model)
            throws InvalidKeySpecException, NoSuchAlgorithmException {
        if (result.hasErrors()) {
            model = initialize(model, session, meeting);
            return MEETING_CREATE_PAGE;
        }
        if (meeting.isNew()) {
            meeting.setCreatedTime(LocalDateTime.now());
        } else {
            meeting.setCreatedTime(meetingDao.getMeetingById(meeting.getId()).getCreatedTime());
        }
        meeting.setLastUpdateTime(LocalDateTime.now());
        if (!meeting.getUrl().contains(HTTP)) {
            String http = HTTP;
            http += meeting.getUrl();
            meeting.setUrl(http);
        }
        meetingDao.save(meeting);
        User activeUser = (User) session.getAttribute(USER);
        User user = userDao.getUserById(activeUser.getId());
        List<Meeting> createdMeetings = user.getCreatedMeetings();
        if (!createdMeetings.contains(meeting)) {
            createdMeetings.add(meeting);
        }
        userDao.save(user);
        return REDIRECT_CREATED_MEETING;
    }
}
