package net.therap.workspace.controller;

import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.Designation;
import net.therap.workspace.domain.User;
import net.therap.workspace.editor.DesignationEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDateTime;

/**
 * @author shahriar.shovon
 * @since 11/29/20
 */
@Controller
public class ProfileController {

    private static final String USER = "currentUser";
    private static final String USER_PROFILE_PAGE = "userProfile";
    private static final String VIEW_PROFILE_PAGE = "profileView";
    private static final String UPLOAD_DIRECTORY = "/images";
    private static final String REDIRECT_HOME = "redirect:/home";

    @Autowired
    private UserDao userDao;

    @Autowired
    private DesignationEditor designationEditor;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Designation.class, designationEditor);
    }

    @RequestMapping("/profile")
    public String editProfile(HttpSession session, ModelMap model) {
        User user = (User) session.getAttribute(USER);
        user = userDao.getUserById(user.getId());
        model.addAttribute("user", user);
        return USER_PROFILE_PAGE;
    }

    @RequestMapping(value = "/saveProfile", method = RequestMethod.POST)
    public String saveProfile(User user, BindingResult result, ModelMap model, @RequestParam("file") CommonsMultipartFile file,
                              HttpSession session) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        if (result.hasErrors()) {
            model.addAttribute("user", user);
            return USER_PROFILE_PAGE;
        }
        if (!file.isEmpty()) {
            ServletContext context = session.getServletContext();
            String path = context.getRealPath(UPLOAD_DIRECTORY);
            String filename = file.getOriginalFilename();
            byte[] bytes = file.getBytes();
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(
                    new File(path + File.separator + filename)));
            stream.write(bytes);
            stream.flush();
            stream.close();
            user.setPicture(UPLOAD_DIRECTORY + "/" + filename);
        }
        user.setLastUpdater(user);
        user.setLastUpdateDate(LocalDateTime.now());
        userDao.save(user);
        return REDIRECT_HOME;
    }

    @RequestMapping("/viewProfile")
    public String viewProfile(User user, ModelMap model, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        user = userDao.getUserById(user.getId());
        model.addAttribute("user", user);
        model.addAttribute("currentUser", currentUser);
        return VIEW_PROFILE_PAGE;
    }
}
