package net.therap.workspace.controller;

import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.Task;
import net.therap.workspace.domain.User;
import net.therap.workspace.editor.DateTimeEditor;
import net.therap.workspace.editor.IntegerEditor;
import net.therap.workspace.editor.UserListEditor;
import net.therap.workspace.service.TaskService;
import net.therap.workspace.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author aunabil.chakma
 * @since 21/02/2021
 **/
@Controller
@SessionAttributes("task")
@RequestMapping("/task")
public class TaskController {

    private static final String TASK = "task";
    private static final String TASK_LIST = "taskList";
    private static final String TASK_SINGLE = "taskSingle";
    private static final String REDIRECT_TASK_ASSIGNED = "redirect:/task/assignedTask";
    private static final int DELETE_STATUS = 1;
    private static final int IS_COMPLETED = 1;
    private static final int NOT_IS_COMPLETED = 0;

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private DateTimeEditor dateTimeEditor;

    @Autowired
    private TeamService teamService;

    @Autowired
    private UserListEditor userListEditor;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        dataBinder.registerCustomEditor(LocalDateTime.class, "deadline", dateTimeEditor);
        dataBinder.registerCustomEditor(List.class, "assignedUsers", userListEditor);
        dataBinder.registerCustomEditor(Integer.class, "isCompleted", new IntegerEditor());
    }

    @GetMapping("/currentTask")
    public String current(Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        model.addAttribute("currentUser", currentUser);

        model.addAttribute("type", "current");
        model.addAttribute("taskList", taskService.findByIsCompleted(NOT_IS_COMPLETED, currentUser));

        return TASK_LIST;
    }

    @GetMapping("/completedTask")
    public String completed(Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        model.addAttribute("currentUser", currentUser);

        model.addAttribute("type", "completed");
        model.addAttribute("taskList", taskService.findByIsCompleted(IS_COMPLETED, currentUser));

        return TASK_LIST;
    }

    @GetMapping("/assignedTask")
    public String assigned(Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        model.addAttribute("currentUser", currentUser);

        model.addAttribute("type", "assigned");
        model.addAttribute("taskList", taskService.findAssigned(currentUser));

        return TASK_LIST;
    }

    @GetMapping("/show")
    public String show(@RequestParam String type, @RequestParam int id, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        model.addAttribute("currentUser", currentUser);

        Task task = taskService.findById(id);
        model.addAttribute("task", task);
        model.addAttribute("type", type);

        return TASK_SINGLE;
    }

    @GetMapping
    public String createOrUpdate(@RequestParam String action, @RequestParam int id, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        model.addAttribute("currentUser", currentUser);

        Task task = (id == 0) ? new Task() : taskService.findById(id);

        List<User> userList = userDao.getUsers();
        userList.removeIf(user -> currentUser.getId() == user.getId());

        model.addAttribute("task", task);
        model.addAttribute("action", action);
        model.addAttribute("userList", userList);

        return TASK;
    }

    @PostMapping
    public String saveOrUpdate(@RequestParam String action, @Valid Task task, BindingResult bindingResult, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        model.addAttribute("currentUser", currentUser);

        if (bindingResult.hasErrors()) {
            model.addAttribute("task", task);
            model.addAttribute("action", action);
            model.addAttribute("userList", userDao.getUsers());
            return TASK;
        }

        if (task.isNew()) {
            task.setCreatedBy(currentUser);
            task.setCreatedDate(LocalDateTime.now());
        } else {
            task.setLastUpdatedBy(currentUser);
            task.setLastUpdatedDate(LocalDateTime.now());
        }

        taskService.saveOrUpdate(task);
        return REDIRECT_TASK_ASSIGNED;
    }

    @PostMapping(params = "action_delete")
    public String delete(@RequestParam int id, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        model.addAttribute("currentUser", currentUser);

        Task task = taskService.findById(id);
        task.setLastUpdatedBy(currentUser);
        task.setLastUpdatedDate(LocalDateTime.now());
        task.setDeletedBy(currentUser);
        task.setDeletedDate(LocalDateTime.now());
        task.setStatus(DELETE_STATUS);

        taskService.saveOrUpdate(task);
        return REDIRECT_TASK_ASSIGNED;
    }
}
