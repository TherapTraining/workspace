package net.therap.workspace.controller;

import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.Task;
import net.therap.workspace.domain.Team;
import net.therap.workspace.domain.User;
import net.therap.workspace.editor.DateTimeEditor;
import net.therap.workspace.editor.UserEditor;
import net.therap.workspace.editor.UserListEditor;
import net.therap.workspace.service.TaskService;
import net.therap.workspace.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * @author aunabil.chakma
 * @since 22/02/2021
 **/
@Controller
@SessionAttributes("task")
@RequestMapping("/team")
public class TeamController {

    private static final String TEAM_TASK_SINGLE = "teamTaskSingle";
    private static final String TEAM_TASK = "teamTask";
    private static final String TEAM_TASK_LIST = "teamTaskList";
    private static final String REDIRECT_TEAM_TASK_LIST = "redirect:/team/task/list";
    private static final String TEAM_SINGLE = "teamSingle";
    private static final int DELETE_STATUS = 1;
    private static final String CURRENT_USER = "currentUser";

    @Autowired
    private TeamService teamService;

    @Autowired
    private UserDao userDao;

    @Autowired
    private TaskService taskService;

    @Autowired
    private DateTimeEditor dateTimeEditor;

    @Autowired
    private UserListEditor userListEditor;

    @Autowired
    private UserEditor userEditor;

    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        dataBinder.registerCustomEditor(List.class, new StringTrimmerEditor(true));
        dataBinder.registerCustomEditor(LocalDateTime.class, "deadline", dateTimeEditor);
        dataBinder.registerCustomEditor(List.class, "assignedUsers", userListEditor);
        dataBinder.registerCustomEditor(User.class, "teamLead", userEditor);
    }

    @GetMapping("/show")
    public String showTeam(Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        model.addAttribute(CURRENT_USER, currentUser);

        Team team = currentUser.getLedTeam();
        if (Objects.isNull(team)) {
            team = currentUser.getTeam();
        }

        model.addAttribute("team", team);
        return TEAM_SINGLE;
    }

    @GetMapping("/task/list")
    public String showTasks(Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        model.addAttribute(CURRENT_USER, currentUser);

        Team team = currentUser.getLedTeam();
        if (Objects.isNull(team)) {
            team = currentUser.getTeam();
        }
        model.addAttribute("team", team);

        model.addAttribute("taskList", teamService.getTaskListUser(team, currentUser));
        return TEAM_TASK_LIST;
    }

    @GetMapping("/task/show")
    public String show(@RequestParam int id, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        model.addAttribute(CURRENT_USER, currentUser);

        Team team = currentUser.getLedTeam();
        if (Objects.isNull(team)) {
            team = currentUser.getTeam();
        }
        model.addAttribute("team", team);

        Task task = taskService.findById(id);
        model.addAttribute("task", task);

        return TEAM_TASK_SINGLE;
    }

    @GetMapping("/task")
    public String createOrUpdate(@RequestParam String action, @RequestParam int id, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        model.addAttribute(CURRENT_USER, currentUser);

        Team team = currentUser.getLedTeam();
        model.addAttribute("team", team);

        Task task;
        if (id == 0) {
            task = new Task();
            task.setTeam(team);
        } else {
            task = taskService.findById(id);
        }

        model.addAttribute("task", task);
        model.addAttribute("action", action);
        model.addAttribute("userList", team.getMemberUsers());

        return TEAM_TASK;
    }

    @PostMapping("/task")
    public String saveOrUpdate(@RequestParam String action, @Valid Task task, BindingResult bindingResult, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        model.addAttribute(CURRENT_USER, currentUser);

        Team team = currentUser.getLedTeam();
        model.addAttribute("team", team);

        if (bindingResult.hasErrors()) {
            model.addAttribute("task", task);
            model.addAttribute("action", action);
            model.addAttribute("userList", team.getMemberUsers());
            return TEAM_TASK;
        }

        if (task.isNew()) {
            task.setCreatedBy(currentUser);
            task.setCreatedDate(LocalDateTime.now());
        } else {
            task.setLastUpdatedBy(currentUser);
            task.setLastUpdatedDate(LocalDateTime.now());
        }

        taskService.saveOrUpdate(task);
        return REDIRECT_TEAM_TASK_LIST;
    }

    @PostMapping(value = "/task", params = "action_delete")
    public String delete(@RequestParam int id, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        model.addAttribute(CURRENT_USER, currentUser);

        Team team = currentUser.getLedTeam();
        model.addAttribute("team", team);

        Task task = taskService.findById(id);
        task.setLastUpdatedBy(currentUser);
        task.setLastUpdatedDate(LocalDateTime.now());
        task.setDeletedBy(currentUser);
        task.setDeletedDate(LocalDateTime.now());
        task.setStatus(DELETE_STATUS);

        taskService.saveOrUpdate(task);
        return REDIRECT_TEAM_TASK_LIST;
    }
}