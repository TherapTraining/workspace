package net.therap.workspace.controller;

import net.therap.workspace.dao.UserDao;
import net.therap.workspace.domain.Message;
import net.therap.workspace.domain.User;
import net.therap.workspace.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * @author aunabil.chakma
 * @since 03/03/2021
 **/
@Controller
@RequestMapping("/message")
public class MessageController {

    private static final String CHAT = "chat";
    private static final String INBOX = "inbox";
    private static final String CURRENT_USER = "currentUser";
    private static final String OTHER_USER = "otherUser";

    @Autowired
    private MessageService messageService;

    @Autowired
    private UserDao userDao;

    @GetMapping("/inbox")
    public String list(Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        model.addAttribute(CURRENT_USER, currentUser);
        model.addAttribute("userList", userDao.getUsers());
        return INBOX;
    }

    @GetMapping("/{userId}")
    public String chat(@PathVariable int userId, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        User otherUser = userDao.getUserById(userId);
        model.addAttribute(CURRENT_USER, currentUser);
        model.addAttribute(OTHER_USER, otherUser);

        model.addAttribute("messageList", messageService.getChat(currentUser, otherUser));
        model.addAttribute("message", new Message());
        return CHAT;
    }

    @PostMapping("/{userId}")
    public String process(@PathVariable int userId, @Valid Message message, BindingResult bindingResult, Model model, HttpSession session) {
        User currentUser = (User) session.getAttribute(CURRENT_USER);
        User otherUser = userDao.getUserById(userId);
        model.addAttribute(CURRENT_USER, currentUser);
        model.addAttribute(OTHER_USER, otherUser);

        if (bindingResult.hasErrors()) {
            model.addAttribute("messageList", messageService.getChat(currentUser, otherUser));
            model.addAttribute("message", message);
            return CHAT;
        }

        message.setCreatedBy(currentUser);
        message.setCreatedDate(LocalDateTime.now());
        message.setSender(currentUser);
        message.setReceiver(otherUser);
        message.setTime(LocalDateTime.now());

        messageService.saveOrUpdate(message);
        return redirectChatUrl(userId);
    }

    private String redirectChatUrl(int id) {
        return "redirect:/message/" + id;
    }
}