<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<form:form action="saveDesignation" modelAttribute="designation" method="post">
    <label><b><fmt:message key="label.name"/></b><br></label><br>
    <form:input type="text" path="name"/><br>
    <form:errors path="name" cssStyle="color: crimson"/><br>
    <button type="submit" class="btn btn-primary" value="Confirm">
        <fmt:message key="label.confirm"/>
    </button>
</form:form>
</body>
</html>
