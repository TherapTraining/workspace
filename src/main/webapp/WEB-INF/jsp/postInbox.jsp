<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<ol>
    <c:forEach var="post" items="${posts}">
        <li>
            <h1><a href="<c:url value="viewSinglePost?id=${post.id}"/>"><c:out value="${post.title}"/></a></h1><br>
            <c:out value="From:"/><br>
            <h2><c:out value="${post.creator.name}"/></h2><br>
            <c:out value="${post.createdTime}"/><br>
        </li>
    </c:forEach>
</ol>
</body>
</html>
