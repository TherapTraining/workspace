<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<c:forEach var="user" items="${users}">
    <a href="<c:url value="/admin/createUser?id=${user.id}"/>" style="color: blue"><c:out value="${user.name}"/></a>
    <form:form action="deleteUser?id=${user.id}" method="post">
        <input type="submit" value="<fmt:message key="label.delete"/>"/>
    </form:form><br><br>
</c:forEach>
</body>
</html>
