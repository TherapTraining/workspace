<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<form:form action="savePost?id=${post.id}" modelAttribute="post" method="post" enctype="multipart/form-data">
    <label><b><fmt:message key="label.title"/></b><br></label><br>
    <form:textarea cssStyle="padding: 0 0 50px;; width: 100%" type="text" path="title"/><br>
    <form:errors path="title" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.body"/></b><br></label><br>
    <form:textarea cssStyle="padding: 0 0 230px;; width: 100%" type="text" path="body"/><br>
    <form:errors path="body" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.file"/></b><br></label><br>
    <c:choose>
        <c:when test="${post.filePath == null}">
            <input type="file" name="file"/><br>
        </c:when>
        <c:otherwise>
            <c:out value="${fileName}"/><br>
            <a href="<c:url value="deletePostFile?id=${post.id}&fileName=${fileName}&filePath=${filePath}"/>">Remove</a><br>
        </c:otherwise>
    </c:choose><br>
    <label><b><fmt:message key="label.tag"/></b><br></label><br>
    <form:select path="taggedUsers" itemLabel="name" itemValue="id">
        <c:forEach var="user" items="${users}">
            <c:choose>
                <c:when test="${post.taggedUsers.contains(user)}">
                    <form:option selected="true" value="${user.id}"><c:out value="${user.name}"/></form:option>
                </c:when>
                <c:otherwise>
                    <form:option value="${user.id}"><c:out value="${user.name}"/></form:option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </form:select>
    <form:hidden path="creator" value="${user.id}"/><br>
    <form:hidden path="lastUpdater" value="${user.id}"/><br>
    <form:hidden path="status" value="${status}"/><br>
    <button type="submit" class="btn btn-primary" value="Confirm">
        <fmt:message key="label.confirm"/>
    </button>
</form:form>
</body>
</html>
