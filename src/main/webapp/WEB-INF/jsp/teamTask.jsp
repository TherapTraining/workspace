<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
   @author aunabil.chakma
   @since 02/03/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Team Task</title>
</head>
<body>

<c:url value="/team/task" var="createOrUpdateLink"/>
<form:form action="${createOrUpdateLink}" method="post" modelAttribute="task">

    <form:hidden path="id"/>
    <b><fmt:message key="label.description"/>:</b><br>
    <form:textarea cssStyle="height: 30%; width: 100%;" type="text" path="description"/>
    <form:errors path="description"/>
    <br><br>

    <b><fmt:message key="label.deadline"/>:</b><br>
    <form:input type="datetime-local" path="deadline"/>
    <form:errors path="deadline"/>
    <br><br>

    <b><fmt:message key="label.completed"/>:</b><br>
    <form:select path="isCompleted">
        <form:option value="1"><fmt:message key="label.true"/></form:option>
        <form:option value="0"><fmt:message key="label.false"/></form:option>
    </form:select>
    <form:errors path="isCompleted"/>
    <br><br>

    <b><fmt:message key="label.assignedUsers"/>:</b><br>
    <form:select path="assignedUsers" itemLabel="name" itemValue="id" multiple="true">
        <c:forEach var="user" items="${userList}">
            <c:choose>
                <c:when test="${task.assignedUsers.contains(user)}">
                    <form:option value="${user.id}" selected="true"><c:out value="${user.name}"/></form:option>
                </c:when>
                <c:otherwise>
                    <form:option value="${user.id}"><c:out value="${user.name}"/></form:option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </form:select>
    <form:errors path="assignedUsers"/>
    <br><br>

    <button type="submit" class="btn btn-primary" value="${action}" name="action">
        <c:out value="${action}"/>
    </button>
</form:form>
</body>
</html>
