<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
   @author aunabil.chakma
   @since 21/02/2021
--%>
<html>
<head>
    <title>Workspace</title>
</head>
<body>


<c:url var="createLink" value="./">
    <c:param name="action" value="create"/>
    <c:param name="id" value="0"/>
</c:url>
<button type="button" class="btn btn-primary">
    <a href="${createLink}"><fmt:message key="label.createNewTask"/></a>
</button>
<br>

<table style="width:100%">
    <tr>
        <th>Task Description</th>
        <th>Creator</th>
        <th>Completion Status</th>
    </tr>

    <c:if test="${taskList.size()==0}">
        <tr>
            <td><c:out value="N/A"/></td>
            <td>-</td>
            <td>-</td>
        </tr>
    </c:if>
    <c:forEach var="task" items="${taskList}">
        <tr>
            <c:url var="showLink" value="/task/show">
                <c:param name="id" value="${task.id}"/>
                <c:param name="type" value="${type}"/>
            </c:url>
            <Td>
                <a href="${showLink}"> <c:out value="${task}"/> </a>
            </Td>
            <Td>
                <a href="${showLink}"> <c:out value="${task.createdBy}"/> </a>
            </Td>
            <Td>
                <c:choose>
                    <c:when test="${task.isCompleted==0}">
                        <a href="${showLink}"> <c:out value="no"/> </a>
                    </c:when>
                    <c:otherwise>
                        <a href="${showLink}"> <c:out value="yes"/> </a>
                    </c:otherwise>
                </c:choose>
            </Td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
