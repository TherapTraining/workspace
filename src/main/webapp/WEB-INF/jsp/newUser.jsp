<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<form:form action="saveUser?id=${user.id}" modelAttribute="user" method="post">
    <label><b><fmt:message key="label.name"/></b><br></label><br>
    <form:input type="text" path="name"/><br>
    <form:errors path="name" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.email"/></b><br></label><br>
    <form:input type="text" path="email"/><br>
    <form:errors path="email" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.password"/></b><br></label><br>
    <form:input path="password" type="text"/><br>
    <form:errors path="password" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.designation"/></b><br></label><br>
    <form:select path="designation" itemLabel="name" itemValue="id" items="${designations}"/><br><br>
    <label><b><fmt:message key="label.isAdmin"/></b><br></label><br>
    <form:select path="isAdmin">
        <form:option value="1"><fmt:message key="label.true"/></form:option>
        <form:option value="0"><fmt:message key="label.false"/></form:option>
    </form:select><br><br>

    <button type="submit" class="btn btn-primary" value="Confirm">
        <fmt:message key="label.confirm"/>
    </button>
</form:form>
</body>
</html>
