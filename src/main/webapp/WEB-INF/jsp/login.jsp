<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
   @author aunabil.chakma
   @since 25/02/2021
--%>
<html>
<head>
    <title>Workspace</title>
</head>
<body>

<div class="login-forms">
    <form:form action="login" modelAttribute="user" method="post">
        <form:hidden path="id"/>

        <fmt:message key="label.email"/>:
        <form:input path="email"/>
        <form:errors path="email"/>
        <br><br>

        <fmt:message key="label.password"/>:
        <form:password path="password"/>
        <form:errors path="password"/>
        <br><br>
        <button type="submit" class="btn btn-primary">
            <fmt:message key="label.login"/>
        </button>
    </form:form>
</div>
<a href="<c:url value="/?locale=bn"/>" style="color: blue"><fmt:message key="label.bengali"/></a>
<a href="<c:url value="/?locale=en"/>" style="color: blue"><fmt:message key="label.english"/></a>
</body>
</html>
