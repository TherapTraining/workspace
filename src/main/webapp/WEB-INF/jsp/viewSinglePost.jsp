<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<h1><b><c:out value="${post.title}"/></b></h1><br>
<b><c:out value="From:"/></b><br>
<h2><c:out value="${post.creator.name}"/></h2><br>
<pre><c:out value="${post.body}"/></pre>
<a href="<c:url value="downloadPostFile?id=${post.id}&fileName=${fileName}&filePath=${filePath}"/>"><c:out
        value="${fileName}"/></a><br>
<c:out value="${post.createdTime}"/><br><br>
<b><c:out value="Tagged:"/></b><br>
<c:set var="count" value="${1}"/>
<c:forEach var="user" items="${post.taggedUsers}">
    <c:if test="${count>1}">
        ,
    </c:if>
    <c:url value="/viewProfile?id=${user.id}" var="profileLink"/>
    <a href="${profileLink}" style="color: blue">
        <c:out value="${user.name}"/>
    </a>
    <c:set var="count" value="${count+1}"/>
</c:forEach>
</body>
</html>
