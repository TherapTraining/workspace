<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
   @author aunabil.chakma
   @since 24/02/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Team Task List</title>
</head>
<body>

<c:if test="${currentUser==team.teamLead}">
    <c:url var="createLink" value="/team/task">
        <c:param name="action" value="create"/>
        <c:param name="id" value="0"/>
    </c:url>
    <button type="button" class="btn btn-primary">
        <a href="${createLink}"><fmt:message key="label.createNewTask"/></a>
    </button>
    <br><br>
</c:if>

<table style="width:100%">
    <tr>
        <th>Task Description</th>
        <th>Completion Status</th>
    </tr>

    <c:if test="${taskList.size()==0}">
        <tr>
            <td>null</td>
            <td>null</td>
            <td>null</td>
        </tr>
    </c:if>
    <c:forEach var="task" items="${taskList}">
        <tr>
            <c:url var="showLink" value="/team/task/show">
                <c:param name="id" value="${task.id}"/>
            </c:url>
            <Td>
                <a href="${showLink}"> <c:out value="${task}"/> </a>
            </Td>
            <Td>
                <c:choose>
                    <c:when test="${task.isCompleted==0}">
                        <a href="${showLink}"> <c:out value="no"/> </a>
                    </c:when>
                    <c:otherwise>
                        <a href="${showLink}"> <c:out value="yes"/> </a>
                    </c:otherwise>
                </c:choose>
            </Td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
