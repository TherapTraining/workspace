<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<ul>
    <c:forEach var="post" items="${posts}">
        <li><h1><b><a href="postCreate?id=${post.id}" style="text-decoration: none">${post.title}</a></b></h1>
            <pre><c:out value="${post.body}"/></pre>
            <b><fmt:message key="label.createdTime"/></b><br>
            <c:out value="${post.createdTime}"/><br><br>

            <b><fmt:message key="label.sentTo"/></b><br>
            <c:set var="count" value="${1}"/>
            <c:forEach var="user" items="${post.taggedUsers}">
                <c:if test="${count>1}">
                    ,
                </c:if>
                <c:url value="/viewProfile?id=${user.id}" var="profileLink"/>
                <a href="${profileLink}" style="color: blue">
                    <c:out value="${user.name}"/>
                </a>
                <c:set var="count" value="${count+1}"/>
            </c:forEach><br><br>


            <c:choose>
                <c:when test="${post.status==1}">
                    <b><fmt:message key="label.active"/></b><br><br>
                    <b><fmt:message key="label.lastUpdate"/></b><br>
                    <c:out value="${post.lastUpdateDate}"/><br>
                    <form:form action="deletePost?id=${post.id}" method="post">
                        <button type="submit" class="btn btn-primary" value="Delete">
                            Delete
                        </button>
                    </form:form>
                </c:when>
                <c:otherwise>
                    <b><fmt:message key="label.deleted"/></b><br><br>
                    <b><fmt:message key="label.deletedAt"/></b><br>
                    <c:out value="${post.deletedTime}"/><br>
                </c:otherwise>
            </c:choose>
        </li>
    </c:forEach>
</ul>
</body>
</html>
