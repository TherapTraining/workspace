<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
   @author aunabil.chakma
   @since 03/03/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chat</title>
</head>
<body>

<c:url var="messageLink" value="/message/${otherUser.id}"/>

<div class="messageListBox">
    <div class="messageBox">
        <c:forEach var="messages" items="${messageList}">
            <c:out value="${messages.sender}"/> : <c:out value="${messages.time}"/><br>
            <c:out value="${messages}"/>

            <br><br>
        </c:forEach>
    </div>
</div>
<form:form method="post" modelAttribute="message" action="${messageLink}" autocomplete="off">
    <form:hidden path="id"/>
    <form:errors path="id"/>

    <form:input cssStyle="border-radius: 10px; width: 35%;border-color: #6d7fcc;border-width: 5px;" path="text"/>
    <form:errors path="text"/>

    <c:url value="/images/sendMessageIcon.png" var="sendIconLink"/>
    <button style="border: none; border-radius: 7px;">
        <img src="${sendIconLink}" style="height: 30px; width: 30px"/>
    </button>

</form:form>
<form:errors path="message"/>

</body>
</html>
