<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<c:choose>
    <c:when test="${user.isAdmin == 1}">
        <a href="<c:url value="/admin/createUser?id=0"/>" style="color: blue"><fmt:message key="label.createUser"/><br></a>
        <a href="<c:url value="/admin/activeUsers"/>" style="color: blue"><fmt:message key="label.viewUsers"/><br></a>
        <a href="<c:url value="/admin/createDesignation"/>" style="color: blue"><fmt:message
                key="label.createDesignation"/><br></a>
    </c:when>
    <c:otherwise>
        <fmt:message key="label.restricted"/><br>
    </c:otherwise>
</c:choose>
<a href="<c:url value="/postInbox"/>" style="color: blue"><fmt:message key="label.posts"/><br></a>
<a href="<c:url value="/message/inbox"/>" style="color: blue"><fmt:message key="label.messages"/><br></a>
<a href="<c:url value="/task/currentTask"/>" style="color: blue"><fmt:message key="label.tasks"/><br></a>
<a href="<c:url value="/invitedMeeting"/>" style="color: blue"><fmt:message key="label.meeting"/><br></a>
<a href="<c:url value="/team/show"/>" style="color: blue"><fmt:message key="label.teamShow"/><br></a>
<a href="<c:url value="/logout"/>" style="color: blue"><fmt:message key="label.logout"/><br></a>
</body>
</html>