<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<form:form action="saveMeeting?id=${meeting.id}" modelAttribute="meeting" method="post">
    <label><b><fmt:message key="label.title"/></b><br></label><br>
    <form:textarea cssStyle="padding: 0 0 50px;; width: 100%" type="text" path="title"/><br>
    <form:errors path="title" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.agenda"/></b><br></label><br>
    <form:textarea cssStyle="padding: 0 0 230px;; width: 100%" type="text" path="agenda"/><br>
    <form:errors path="agenda" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.time"/>:</b><br></label><br>
    <form:input path="time" type="datetime-local"/><br>
    <form:errors path="time" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.url"/>:</b><br></label><br>
    <form:input path="url" type="text"/><br>
    <label><b><fmt:message key="label.invite"/></b><br></label><br><br>
    <form:select path="guests" itemLabel="name" itemValue="id">
        <c:forEach var="user" items="${users}">
            <c:choose>
                <c:when test="${meeting.guests.contains(user)}">
                    <form:option selected="true" value="${user.id}">${user.name}</form:option>
                </c:when>
                <c:otherwise>
                    <form:option value="${user.id}">${user.name}</form:option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </form:select>
    <form:hidden path="creator" value="${user.id}"/><br>
    <form:hidden path="lastUpdater" value="${user.id}"/><br>
    <form:hidden path="status" value="${status}"/><br>
    <button type="submit" class="btn btn-primary">
        <fmt:message key="label.confirm"/>
    </button>
</form:form>
</body>
</html>
