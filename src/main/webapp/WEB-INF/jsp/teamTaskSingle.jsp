<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 02/03/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Team Task</title>
</head>
<body>

<b><fmt:message key="label.description"/>:</b>
<c:out value="${task.description}"/><br><br>

<c:set var="count" value="${1}"/>
<b><fmt:message key="label.assignedUsers"/>:</b>
<c:forEach var="user" items="${task.assignedUsers}">
    <c:if test="${count>1}">
        ,
    </c:if>
    <c:url value="/viewProfile?id=${user.id}" var="profileLink"/>
    <a href="${profileLink}" style="color: blue">
        <c:out value="${user.name}"/>
    </a>
    <c:set var="count" value="${count+1}"/>
</c:forEach><br><br>

<b><fmt:message key="label.completed"/>:</b>
<c:choose>
    <c:when test="${task.isCompleted==0}">
        <c:out value="no"/><br><br>
    </c:when>
    <c:otherwise>
        <c:out value="yes"/><br><br>
    </c:otherwise>
</c:choose>


<b><fmt:message key="label.deadline"/>:</b>
<c:out value="${task.deadline}"/><br><br>

<c:if test="${currentUser==team.teamLead}">
    <c:url var="updateLink" value="/team/task">
        <c:param name="action" value="update"/>
        <c:param name="id" value="${task.id}"/>
    </c:url>
    <button type="button" class="btn btn-primary">
        <a href="${updateLink}"><fmt:message key="label.update"/></a>
    </button>


    <c:url var="deleteLink" value="/team/task">
        <c:param name="id" value="${task.id}"/>
    </c:url>
    <form action="${deleteLink}" method="post">
        <button type="submit" class="btn btn-primary" value="delete" name="action_delete">
            <fmt:message key="label.delete"/>
        </button>
    </form>
</c:if>


</body>
</html>
