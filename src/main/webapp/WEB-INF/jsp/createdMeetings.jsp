<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<ul>
    <c:forEach var="meeting" items="${meetings}">
        <li><h1><b><a href="meetingCreate?id=${meeting.id}" style="text-decoration: none">${meeting.title}</a></b></h1>
            <pre><c:out value="${meeting.agenda}"/></pre>
            <b><fmt:message key="label.createdTime"/></b><br>
            <c:out value="${meeting.createdTime}"/><br><br>
            <b><fmt:message key="label.guests"/></b><br>
            <c:set var="count" value="${1}"/>
            <c:forEach var="user" items="${meeting.guests}">
                <c:if test="${count>1}">
                    ,
                </c:if>
                <c:url value="/viewProfile?id=${user.id}" var="profileLink"/>
                <a href="${profileLink}" style="color: blue">
                    <c:out value="${user.name}"/>
                </a>
                <c:set var="count" value="${count+1}"/>
            </c:forEach><br><br>
            <c:choose>
                <c:when test="${meeting.status==1}">
                    <b><fmt:message key="label.active"/></b><br><br>
                    <b><fmt:message key="label.lastUpdate"/></b><br>
                    <c:out value="${meeting.lastUpdateTime}"/><br>
                    <form:form action="deleteMeeting?id=${meeting.id}" method="post">
                        <button type="submit" class="btn btn-primary" value="Delete">
                            <fmt:message key="label.delete"/>
                        </button>
                    </form:form>
                </c:when>
                <c:otherwise>
                    <b><fmt:message key="label.deleted"/></b><br><br>
                    <b><fmt:message key="label.deletedAt"/></b><br>
                    <c:out value="${meeting.deletedTime}"/><br>
                </c:otherwise>
            </c:choose>
        </li>
    </c:forEach>
</ul>
</body>
</html>
