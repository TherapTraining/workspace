<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
   @author aunabil.chakma
   @since 24/02/2021
--%>
<html>
<head>
    <title>Workspace</title>
</head>
<body>

<b><fmt:message key="label.creator"/>:</b>
<c:url value="/viewProfile?id=${task.createdBy.id}" var="profileLink"/>
<a href="${profileLink}" style="color: blue">
    <c:out value="${task.createdBy.name}"/>
</a>
<br><br>

<b><fmt:message key="label.description"/>:</b>
<c:out value="${task.description}"/><br><br>

<c:set var="count" value="${1}"/>
<b><fmt:message key="label.assignedUsers"/>:</b>
<c:forEach var="user" items="${task.assignedUsers}">
    <c:if test="${count>1}">
        ,
    </c:if>
    <c:url value="/viewProfile?id=${user.id}" var="profileLink"/>
    <a href="${profileLink}" style="color: blue">
        <c:out value="${user.name}"/>
    </a>
    <c:set var="count" value="${count+1}"/>
</c:forEach>
<br><br>

<b><fmt:message key="label.completed"/>:</b>
<c:choose>
    <c:when test="${task.isCompleted==0}">
        <c:out value="no"/><br><br>
    </c:when>
    <c:otherwise>
        <c:out value="yes"/><br><br>
    </c:otherwise>
</c:choose>

<b><fmt:message key="label.deadline"/>:</b>
<c:out value="${task.deadline}"/><br><br>

<c:if test="${type=='assigned'}">
    <c:url var="updateLink" value="./">
        <c:param name="action" value="update"/>
        <c:param name="id" value="${task.id}"/>
    </c:url>
    <button type="button" class="btn btn-primary">
        <a href="${updateLink}"><fmt:message key="label.update"/></a>
    </button>

    <c:url var="deleteLink" value="./">
        <c:param name="id" value="${task.id}"/>
    </c:url>
    <form action="${deleteLink}" method="post">
        <button type="submit" class="btn btn-primary" value="delete" name="action_delete">
            <fmt:message key="label.delete"/>
        </button>
    </form>
</c:if>

</body>
</html>