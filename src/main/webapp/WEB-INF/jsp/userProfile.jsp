<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<form:form action="saveProfile?id=${user.id}" modelAttribute="user" method="post" enctype="multipart/form-data">
    <label><b><fmt:message key="label.name"/></b></label><br>
    <form:input type="text" path="name"/><br>
    <form:errors path="name" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.number"/></b></label><br>
    <form:input type="text" path="phone"/><br>
    <form:errors path="phone" cssStyle="color: crimson"/><br>
    <label><b><fmt:message key="label.profilePhoto"/></b></label><br>
    <c:choose>
        <c:when test="${user.picture != null}">
            <c:url value="${user.picture}" var="pictureLink"/>
            <img src="${pictureLink}" alt="<fmt:message key="label.profilePhoto"/>" height="160" width="160"><br>
        </c:when>
        <c:otherwise>
            <fmt:message key="label.noPhoto"/><br>
        </c:otherwise>
    </c:choose>
    <input type="file" name="file"/><br><br>
    <label><b><fmt:message key="label.address"/></b></label><br>
    <form:input type="text" path="address"/>
    <form:errors path="address" cssStyle="color: crimson"/>
    <form:hidden path="email" value="${user.email}"/>
    <form:hidden path="password" value="${user.password}"/>
    <form:hidden path="designation" itemLabel="name" itemValue="id"/>
    <form:hidden path="isAdmin" value="${user.isAdmin}"/>
    <form:hidden path="status" value="${user.status}"/>

    <button type="submit" class="btn btn-primary">
        <fmt:message key="label.confirm"/>
    </button>
</form:form>
</body>
</html>
