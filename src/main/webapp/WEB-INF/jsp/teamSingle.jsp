<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
   @author aunabil.chakma
   @since 22/02/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Team</title>
</head>
<body>

<b><fmt:message key="label.teamName"/>:</b>
<c:out value="${team.name}"/><br><br><br>

<b><fmt:message key="label.teamLead"/>:</b>
<c:url value="/viewProfile?id=${team.teamLead.id}" var="profileLink"/>
<a href="${profileLink}" style="color: blue">
    <c:out value="${team.teamLead.name}"/>
</a> <br><br><br>

<b><fmt:message key="label.members"/>:</b>
<c:set var="count" value="${1}"/>
<c:forEach var="user" items="${team.memberUsers}">
    <c:if test="${count>1}">
        ,
    </c:if>
    <c:url value="/viewProfile?id=${user.id}" var="profileLink"/>
    <a href="${profileLink}" style="color: blue">
        <c:out value="${user.name}"/>
    </a>
    <c:set var="count" value="${count+1}"/>
</c:forEach>

</body>
</html>
