<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
   @author aunabil.chakma
   @since 03/03/2021
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Inbox</title>
</head>
<body>

<b><fmt:message key="label.contact"/>:</b><br><br>

<c:forEach var="user" items="${userList}">
    <c:if test="${user!=currentUser}">
        <c:url var="chatLink" value="/message/${user.id}"/>
        <a href="${chatLink}"> <c:out value="${user.name}"/> </a>
        <br><br>
    </c:if>
</c:forEach>

</body>
</html>
