<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<c:choose>
    <c:when test="${user.picture != null}">
        <c:url value="${user.picture}" var="pictureLink"/>
        <img src="${pictureLink}" alt="<fmt:message key="label.profilePhoto"/>" height="160" width="160"><br>
    </c:when>
    <c:otherwise>
        <c:url value="/images/profile.png" var="pictureLink"/>
        <img src="${pictureLink}" alt="<fmt:message key="label.profilePhoto"/>" height="160" width="160"><br>
    </c:otherwise>

</c:choose>
<h1><c:out value="${user.name}"/></h1>
<h3><c:out value="${user.designation.name}"/></h3>
<fmt:message key="label.email"/>: <c:out value="${user.email}"/><br>
<fmt:message key="label.number"/>: <c:out value="${user.phone}"/><br>
<fmt:message key="label.address"/>: <c:out value="${user.address}"/><br>

<c:if test="${currentUser==user}">
    <c:url var="updateLink" value="/profile"/>
    <button type="button" class="btn btn-primary">
        <a href="${updateLink}">update</a>
    </button>
</c:if>

</body>
</html>
