<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<ol>
    <c:forEach var="meeting" items="${meetings}">
        <li>
            <h1><b><c:out value="${meeting.title}"/></b></h1><br>
            <b><c:out value="Host:"/></b><br>
            <h2><c:out value="${meeting.creator.name}"/></h2><br>
            <pre><c:out value="${meeting.agenda}"/></pre>
            <c:out value="${meeting.time}"/><br><br>
            <b><c:out value="Guests:"/></b><br>
            <c:set var="count" value="${1}"/>
            <c:forEach var="user" items="${meeting.guests}">
                <c:if test="${count>1}">
                    ,
                </c:if>
                <c:url value="/viewProfile?id=${user.id}" var="profileLink"/>
                <a href="${profileLink}" style="color: blue">
                    <c:out value="${user.name}"/>
                </a>
                <c:set var="count" value="${count+1}"/>
            </c:forEach><br>
            <a href=${meeting.url}><c:out value="Join"/></a>
        </li>
    </c:forEach>
</ol>
</body>
</html>
