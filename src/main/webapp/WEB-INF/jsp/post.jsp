<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<html>
<head>
    <title>WorkSpace</title>
</head>
<body>
<a href="<c:url value="postInbox"/>"><fmt:message key="label.inbox"/><br></a>
<a href="<c:url value="postSent"/>"><fmt:message key="label.sent"/><br></a>
<a href="<c:url value="postCreate?id=0"/>"><fmt:message key="label.new"/><br></a>
</body>
</html>
