<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
          integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
            integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ"
            crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
            integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY"
            crossorigin="anonymous"></script>

    <c:url value="/css/style.css" var="styleLink"/>

    <link rel="stylesheet" href="${styleLink}"/>

    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>
</head>

<body>

<%--url link--%>
<c:url value="/home" var="homeLink"/>

<c:url value="/task/currentTask" var="taskCurrentLink"/>
<c:url value="/task/completedTask" var="taskCompletedLink"/>
<c:url value="/task/assignedTask" var="taskAssignedLink"/>
<c:url var="createTaskLink" value="/task">
    <c:param name="action" value="create"/>
    <c:param name="id" value="0"/>
</c:url>


<c:url value="/postInbox" var="postInboxLink"/>
<c:url value="/postSent" var="postSentLink"/>
<c:url value="/postCreate" var="postCreateLink"/>

<c:url value="/message/inbox" var="messageInboxLink"/>

<c:url value="/team/task/list" var="teamTaskListLink"/>
<c:url value="/team/show" var="teamShowLink"/>
<c:url var="createTeamTaskLink" value="/team/task">
    <c:param name="action" value="create"/>
    <c:param name="id" value="0"/>
</c:url>

<c:url value="/invitedMeeting" var="invitedMeetings"/>
<c:url value="/meetingCreate?id=0" var="createMeeting"/>
<c:url value="/meetingCreated" var="createdMeetings"/>

<c:url value="/viewProfile?id=${currentUser.id}" var="profileLink"/>

<c:url value="/logout" var="signOutLink"/>

<%--upper nav bar--%>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <%--<nav class="navbar navbar-light bg-light">--%>
    <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fas fa-align-left"></i>
            <span><fmt:message key="label.menus"/></span>
        </button>

        <div class="sidebar-header">
            <h2><b><a href="${homeLink}" style="font-style: italic;color: red;"><fmt:message
                    key="label.projectName"/></a></b></h2>
        </div>

    </div>
</nav>

<%--main content and side bar--%>
<div class="wrapper">
    <%--side nav bar--%>
    <nav id="sidebar">

        <ul class="list-unstyled components">
            <li>
                <a href="${homeLink}"><fmt:message key="label.home"/></a>
            </li>
            <li class="active">
                <a href="#postSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><fmt:message
                        key="label.posts"/></a>
                <ul class="collapse list-unstyled" id="postSubmenu">
                    <li>
                        <a href="${postInboxLink}"><fmt:message key="label.inbox"/></a>
                    </li>
                    <li>
                        <a href="${postSentLink}"><fmt:message key="label.sent"/></a>
                    </li>
                    <li>
                        <a href="${postCreateLink}"><fmt:message key="label.create"/></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#taskSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><fmt:message
                        key="label.tasks"/></a>
                <ul class="collapse list-unstyled" id="taskSubmenu">
                    <li>
                        <a href="${taskCurrentLink}"><fmt:message key="label.current"/></a>
                    </li>
                    <li>
                        <a href="${taskCompletedLink}"><fmt:message key="label.completed"/></a>
                    </li>
                    <li>
                        <a href="${taskAssignedLink}"><fmt:message key="label.assigned"/></a>
                    </li>
                    <li>
                        <a href="${createTaskLink}"><fmt:message key="label.createNewTask"/></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#messageSubmenu" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle"><fmt:message key="label.messages"/></a>
                <ul class="collapse list-unstyled" id="messageSubmenu">
                    <li>
                        <a href="${messageInboxLink}"><fmt:message key="label.inbox"/></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#teamSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><fmt:message
                        key="label.teams"/></a>
                <ul class="collapse list-unstyled" id="teamSubmenu">
                    <li>
                        <a href="${teamTaskListLink}"><fmt:message key="label.tasks"/></a>
                    </li>
                    <li>
                        <a href="${teamShowLink}"><fmt:message key="label.teamShow"/></a>
                    </li>
                    <li>
                        <c:if test="${currentUser==team.teamLead}">
                            <a href="${createTeamTaskLink}"><fmt:message key="label.createNewTask"/></a>
                        </c:if>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#meetingSubmenu" data-toggle="collapse" aria-expanded="false"
                   class="dropdown-toggle"><fmt:message key="label.meetings"/></a>
                <ul class="collapse list-unstyled" id="meetingSubmenu">
                    <li>
                        <a href="${createdMeetings}"><fmt:message key="label.viewCreated"/></a>
                    </li>
                    <li>
                        <a href="${createMeeting}"><fmt:message key="label.create"/></a>
                    </li>
                    <li>
                        <a href="${invitedMeetings}"><fmt:message key="label.invited"/></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="${profileLink}"> <fmt:message key="label.myProfile"/> ( <c:out value="${currentUser}"/> ) </a>
            </li>
            <li>
                <a href="${signOutLink}"> <fmt:message key="label.signOut"/> </a>
            </li>
        </ul>

    </nav>

    <!-- main Content  -->
    <div id="content">

        <sitemesh:write property='body'/>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"
        integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
        crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>

</html>